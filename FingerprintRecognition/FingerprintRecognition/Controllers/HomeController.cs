﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using FingerprintRecognition.Models;
using System.IO;
using SourceAFIS.Simple;
using System.Drawing;
using System.Web;
using Microsoft.AspNetCore.Http;

namespace FingerprintRecognition.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        static AfisEngine Afis = new AfisEngine();

        static List<Bitmap> staticListBmp = new List<Bitmap>();

        public IActionResult Index()
        {
            try
            {
                staticListBmp.Clear();
                return View();
            }
            catch (Exception ex)
            {
                return View();
            }

        }

        [HttpPost, Route("Result")]
        public IActionResult Result(IFormFile baseFile, List<IFormFile> file)
        {
            List<Person> personsList = new List<Person>();
            List<ResultModel> ListResultModel = new List<ResultModel>();
            Random random = new Random();

            try 
            {

                foreach (IFormFile img in file)
                {

                    staticListBmp.Add(new Bitmap(ConvertIFormFileToImage(img)));
                }

                //Insert the base fingerprint and add her by the first 
                //the base fingerprint will be compared with all the others
                Fingerprint basefp = new Fingerprint();
                basefp.AsBitmap = new Bitmap(ConvertIFormFileToImage(baseFile));
                Person personBase = new Person();
                personBase.Fingerprints.Add(basefp);

                Afis.Extract(personBase);

                //Insert the fingerprints that should be compared on the list
                foreach (Bitmap bmp in staticListBmp)
                {
                    Fingerprint fp = new Fingerprint();
                    fp.AsBitmap = bmp;
                    Person ComparativePerson = new Person();
                    ComparativePerson.Fingerprints.Add(fp);
                    Afis.Extract(ComparativePerson);
                    personsList.Add(ComparativePerson);
                }

                ResultModel rmBase = new ResultModel("baseImage", personBase.Fingerprints[0].AsBitmap, true);
                ListResultModel.Add(rmBase);

                foreach (Person person in personsList)
                {
                    float score = Afis.Verify(personBase, person);
                    bool match = (score > 0);
                    string imageName = random.Next(0, 10000).ToString();
                    ResultModel rm = new ResultModel("Imagem:" + imageName, person.Fingerprints[0].AsBitmap, match);
                    ListResultModel.Add(rm);
                }


                return View(ListResultModel);
            } 
            catch (ArgumentException ex) 
            {
                return View("Index");
            }
        }

        public IActionResult ReturnToIndex()
        {
            staticListBmp.Clear();
            return View("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        private Image ConvertIFormFileToImage(IFormFile file)
        {
            using (var target = new MemoryStream())
            {
                file.CopyTo(target);
                MemoryStream ms = new MemoryStream(target.ToArray());
                return Image.FromStream(ms);
            }
        }
    }

}
