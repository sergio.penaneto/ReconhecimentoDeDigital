﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
using System.IO;

namespace FingerprintRecognition.Models
{
    public class ResultModel
    {
        public Bitmap Image { get; set; }
        public byte[] base64img { get; }
        public string Name { get; set; }
        public bool Result { get; set; }

        public ResultModel(string name, Bitmap img, bool result) 
        {
            this.Image = img;
            this.Name = name;
            this.Result = result;

            MemoryStream ms = new MemoryStream();
            this.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
            this.base64img = ms.ToArray();
        }
    }
}
